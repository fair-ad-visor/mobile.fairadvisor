import 'dart:convert';

import 'package:fairadvisor/api/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Place extends StatefulWidget {
  @override
  _PlaceState createState() => _PlaceState();
}

class _PlaceState extends State<Place> {
  String _title;
  String _description;
  String _address;
  String _type;
  int _id;
  List<Widget> _comments = [];
  bool _connected = false;
  TextEditingController _addReview = TextEditingController();
  int _rating = 1;

  void initState() {
    _getPlace();
    _getUserInfo();
    super.initState();
  }

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    bool res;
    localStorage.getString('token') != null ? res = true : res = false;
    setState(() {
      _connected = res;
    });
  }

  Future<void> _getPlace() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();

    int _placeId = localStorage.getInt("placeId");

    var _header = {
      "Accept": "Application/json",
    };

    var _res = await CallApi().getData("places/$_placeId", _header);
    var _place = json.decode(_res.body);
    _getComments(_id);
    setState(() {
      _id = _placeId;
      _title = _place["name"];
      _type = _place["place_type"]["name"];
      _description = _place["description"];
      _address =
          _place["address"] + ", " + _place["postcode"] + " " + _place["city"];
    });
  }

  Future<void> _getComments(_placeId) async {
    var _header = {
      "Accept": "Application/json",
    };

    String _parameters = 'place_id=$_placeId';

    var _res = await CallApi().getData('review?' + _parameters, _header);
    var _body = json.decode(_res.body);
    if (_body['reviews_count'] != 0) {
      for (var _review in _body['reviews']) {
        Card _res = Card(
          child: ListTile(
            title: _review['owner']["first_name"] +
                " " +
                _review['owner']["last_name"],
            subtitle: _review["comment"],
          ),
        );
        setState(() {
          _comments.add(_res);
        });
      }
    }
  }

  Future<void> _removePlaceId() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove("placeId");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: FaIcon(FontAwesomeIcons.arrowLeft),
          onPressed: () {
            _removePlaceId();
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView(
        children: <Widget>[
          Text("Titre : "),
          Text(_title != null ? _title : ''),
          Text("Type : "),
          Text(_type != null ? _type : ''),
          Text("Adresse : "),
          Text(_address != null ? _address : ''),
          Text("Description : "),
          Text(_description != null ? _description : ''),
          _connected
              ? Column(
                  children: <Widget>[
                    DropdownButton(
                      value: _rating,
                      onChanged: (_value) {
                        print(_rating);
                        setState(() {
                          _rating = _value;
                        });
                        print(_rating);
                      },
                      items: <int>[1, 2, 3, 4, 5]
                          .map<DropdownMenuItem<int>>((int _value) {
                        return DropdownMenuItem<int>(
                          value: _value,
                          child: Text(_value.toString()),
                        );
                      }).toList(),
                    ),
                    TextField(
                      textInputAction: TextInputAction.go,
                      controller: _addReview,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(25.0),
                          ),
                        ),
                        fillColor: Colors.white,
                        filled: true,
                      ),
                    ),
                    FlatButton(
                      child: Text('Envoyer le commentaire'),
                      onPressed: () async {
                        SharedPreferences localStorage =
                            await SharedPreferences.getInstance();

                        String _token = localStorage.getString("token");
                        var _header = {
                          'Accept': 'application/json',
                          'Content-Type': 'application/json',
                          'Authorization': 'Bearer $_token'
                        };

                        var _body = {
                          'place': _id,
                          'rating': _rating,
                          'comment': _addReview.text,
                        };

                        print('send');
                        print(_token);
                        try {
                          var _res =
                              await CallApi().putData(_body, 'review', _header);
                          var _success = json.decode(_res);
                          print(_success);
                          Navigator.pop(context);
                        } catch (e) {
                          throw new Exception(e);
                        }

                        print('sent');
                      },
                    ),
                  ],
                )
              : ListTile(),
          Text('Commentaires :'),
          _comments.length != 0
              ? Column(children: _comments)
              : Text('Pas encore de commentaires'),
        ],
      ),
    );
  }
}
