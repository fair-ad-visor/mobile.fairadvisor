import 'package:fairadvisor/api/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController mailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Email',
              ),
              textInputAction: TextInputAction.next,
            ),
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Password',
              ),
              textInputAction: TextInputAction.go,
            ),
            FlatButton(
              onPressed: _handleLogin,
              child: _isLoading ? Text("Connexion...") : Text("Se connecter!"),
            ),
            FlatButton(
              child: Text("Créer un compte!"),
              onPressed: () {
                Navigator.popAndPushNamed(context, "/signup");
              },
            )
          ],
        ),
      ),
    );
  }

  void _handleLogin() async {
    setState(() {
      _isLoading = true;
    });

    var _header = {
      'content-type': 'application/json',
      'accept': 'application/json',
    };
    
    var _data = {
      'email': mailController,
      'password': passwordController,
    };

    CallApi().postData(_data, "login", _header);
  }
}
