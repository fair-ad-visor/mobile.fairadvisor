import 'dart:convert';

import 'package:fairadvisor/api/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController _mailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Email',
              ),
              controller: _mailController,
              textInputAction: TextInputAction.next,
            ),
            TextFormField(
              decoration: InputDecoration(
                labelText: 'Password',
              ),
              obscureText: true,
              controller: _passwordController,
              textInputAction: TextInputAction.go,
            ),
            FlatButton(
              onPressed: _handleLogin,
              child: _isLoading ? Text("Connexion...") : Text("Se connecter!"),
            ),
            FlatButton(
              onPressed: () {
                Navigator.popAndPushNamed(context, "/signup");
              },
              child: Text("Créer un compte!"),
            )
          ],
        ),
      ),
    );
  }

  void _handleLogin() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'email': _mailController.text,
      'password': _passwordController.text,
      'device_name': 'Login From Application'
    };

    var header = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
    var _res = await CallApi().postData(data, 'auth/login', header);
    var _body = json.decode(_res.body);

    if (_body['token'] != null) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', _body['token']);
      Navigator.popAndPushNamed(context, "/home");
    }

    setState(() {
      _isLoading = false;
    });
  }
}
