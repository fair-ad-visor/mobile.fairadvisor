import 'dart:convert';

import 'package:fairadvisor/api/api.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  var _username;
  var _firstName;
  var _lastName;
  var _email;

  void initState() {
    showProfile();
    super.initState();
  }

  void showProfile() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();

    String _token = localStorage.getString('token');

    var header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $_token'
    };

    var _res = await CallApi().getData('user/me', header);
    var _profile = json.decode(_res.body);

    setState(() {
      _username = _profile['username'];
      _firstName = _profile['first_name'];
      _lastName = _profile['last_name'];
      _email = _profile['email'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        children: <Widget>[
          Text(_username != null ? "Pseudo : " + _username : ''),
          Text(_firstName != null ? "Prénom : " + _firstName : ''),
          Text(_lastName != null ? "Nom : " + _lastName : ''),
          Text(_email != null ? "Mail : " + _email : ''),
        ],
      ),
//      floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
    );
  }
}
