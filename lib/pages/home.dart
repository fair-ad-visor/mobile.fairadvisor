import 'package:fairadvisor/shared/appbar.dart';
import 'package:fairadvisor/shared/menu.dart';
import 'package:flutter/material.dart';

import 'map.dart';

class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: Appbar(),
          drawer: Menu(),
          floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
          body: Map(),
        ),
      ],
    );
  }
}
