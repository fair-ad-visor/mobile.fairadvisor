import 'dart:async';
import 'dart:convert';

import 'package:fairadvisor/api/api.dart';
import 'package:fairadvisor/pages/place.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:latlong/latlong.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_location/user_location.dart';

class Map extends StatefulWidget {
  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<Map> {
  List<Marker> _markers = [];
  int pointIndex;
  MapController _mapController = MapController();
  StreamController<LatLng> _markerlocationStream =
      StreamController<LatLng>.broadcast();
  UserLocationOptions _userLocationOptions;
  int _timer = 0;

  void initState() {
    pointIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _launchingMap = true;

    _markerlocationStream.stream.listen((event) {});

    _userLocationOptions = UserLocationOptions(
      context: context,
      mapController: _mapController,
      markers: _markers,
      onLocationUpdate: (LatLng pos) =>
          print("onLocationUpdate ${pos.toString()}"),
      updateMapLocationOnPositionChange: false,
      showMoveToCurrentLocationFloatingActionButton: true,
      zoomToCurrentLocationOnLoad: false,
      fabBottom: 50,
      fabRight: 50,
      verbose: false,
    );
    return FlutterMap(
      options: MapOptions(
        zoom: 7.0,
        maxZoom: 22.0,
        minZoom: 4.0,
        center: LatLng(48.8566, 2.3522),
        onPositionChanged: (position, hasGesture) {
          Future<void> addMarker() async {
            var header = {
              'Accept': 'application/json',
            };

            var parameter = 'lat_min=' +
                position.bounds.south.toString() +
                '&lon_min=' +
                position.bounds.west.toString() +
                '&lat_max=' +
                position.bounds.north.toString() +
                '&lon_max=' +
                position.bounds.east.toString();

            var _res = await CallApi().getData('places?' + parameter, header);
            var _places = jsonDecode(_res.body);
            for (var _place in _places["places"]) {
              setState(() {
                _markers.add(
                  Marker(
                    width: 40,
                    height: 40,
                    point: LatLng(_place['lat'], _place['lon']),
                    builder: (context) => Container(
                        child: FlatButton(
                      onPressed: () async {
                        SharedPreferences localStorage =
                            await SharedPreferences.getInstance();

                        localStorage.setInt("placeId", _place["id"]);

                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Place(),
                            ));
                      },
                      child: FaIcon(FontAwesomeIcons.hotel),
                    )),
                  ),
                );
              });
            }
          }

          if (_launchingMap || (!hasGesture && _timer > 10)) {
            _launchingMap = false;
            _timer = 0;
            addMarker();
          }
          _timer++;
        },
        plugins: [
          UserLocationPlugin(),
        ],
      ),
      layers: [
        TileLayerOptions(
          urlTemplate:
              'http://tiles.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', //https://account.mapbox.com
        ),
        MarkerLayerOptions(
          markers: _markers,
        ),
        _userLocationOptions,
      ],
      mapController: _mapController,
    );
  }

  void dispose() {
    _markerlocationStream.close();
    super.dispose();
  }
}
