import 'package:fairadvisor/pages/home.dart';
import 'package:fairadvisor/pages/place.dart';
import 'package:fairadvisor/pages/profile.dart';
import 'package:fairadvisor/pages/signUp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

int initScreen;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  initScreen = prefs.getInt("initScreen");
  await prefs.setInt("initScreen", 1);
  print('initScreen $initScreen');
  runApp(MyApp());
}

//void main() {
//  runApp(MyApp());
//}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
      initialRoute: initScreen == 0 || initScreen == null ? "first" : "/",
      routes: <String, WidgetBuilder>{
        "/home": (context) => Home(),
        "/login": (context) => Login(),
        "/profile": (context) => Profile(),
        "/signup": (context) => SignUp(),
        "/place": (context) => Place(),
      },
    );
  }
}
