import 'dart:convert';

import 'package:http/http.dart' as http;

class CallApi {
  final String _url = 'http://fairadvisor.herokuapp.com/api/';

  postData(data, apiUrl, header) async {
    var fullUrl = _url + apiUrl;
    return await http.post(
      fullUrl,
      body: jsonEncode(data),
      headers: header,
    );
  }

  getData(apiUrl, header) async {
    var fullUrl = _url + apiUrl;
    return await http.get(
      fullUrl,
      headers: header,
    );
  }

  putData(data, apiUrl, header) async {
    var fullUrl = _url + apiUrl;
    print('data');
    print(data);
    print('apiUrl');
    print(apiUrl);
    print('fullUrl');
    print(fullUrl);
    print('header');
    print(header);
    return await http.put(
      fullUrl,
      body: jsonEncode(data),
      headers: header,
    );
  }
}
