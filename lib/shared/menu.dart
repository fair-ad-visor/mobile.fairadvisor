import 'package:fairadvisor/pages/profile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Menu extends StatefulWidget {
  @override
  _Menu createState() => _Menu();
}

class _Menu extends State<Menu> {
  @override
  void initState() {
    _getUserInfo();
    super.initState();
  }

  bool _connected = false;

  void _getUserInfo() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    bool res;
    localStorage.getString('token') != null ? res = true : res = false;
    setState(() {
      _connected = res;
    });
  }

  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        // padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            color: Colors.blue,
            child: ListTile(
              title: _connected ? Text('Profile') : Text('Login'),
              leading: FaIcon(FontAwesomeIcons.accessibleIcon),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                _connected
                    ? Navigator.popAndPushNamed(context, "/profile")
                    : Navigator.popAndPushNamed(context, "/login");
                Profile();
              },
            ),
          ),
          _connected
              ? ListTile(
                  title: Text('Disconnect'),
                  onTap: () async {
                    SharedPreferences localStorages =
                        await SharedPreferences.getInstance();

                    localStorages.remove("token");
                    // Update the state of the app
                    // ...
                    // Then close the drawer
                    Navigator.pop(context);
                  },
                )
              : ListTile(),
        ],
      ),
    );
  }
}
