# fairadvisor

Les fonctionnalités en place actuellement :
- Connexion
- Déconnexion
- Affichage de la carte
- Récupération des informations de l'utilisateur connecté
- Récupération et affichage sur la carte des lieux disponible
- Affichage des informations d'un lieux disponible (page disponible au clic sur une des icones de la carte)
- Récupération des avis sur un lieu (pas encore testé)

Les fonctionnalités en cours :
- Envoi d'un avis sur un lieu

Les fonctionnalités futur : 
- Création de compte
- Ajout de commentaires sous un avis 
- Ajout de lieux
- Suppression de lieux
- Modification des informations d'un lieu
- Modification des information de l'utilisateur connecté
- Barre de recherche